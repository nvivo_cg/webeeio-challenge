package com.serverless.dao.impl;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.serverless.api.DeviceResponse;
import com.serverless.dao.DevicesDao;
import com.serverless.model.Device;
import com.serverless.utils.DatasourceException;
import com.serverless.utils.InvalidInputException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by DiegoT on 01/11/2018.
 */
public class DevicesDaoImpl implements DevicesDao {
    private static final Logger LOG = LogManager.getLogger(DevicesDaoImpl.class);

    private static final String devicesTable = System.getenv("DEVICES_TABLE_NAME");

    public List<DeviceResponse> getDevices(String param) {
        List<DeviceResponse> devices = new ArrayList<DeviceResponse>();

        try {
            final AmazonDynamoDB ddb = AmazonDynamoDBClientBuilder.defaultClient();

            ScanRequest request = new ScanRequest().withTableName(devicesTable);

            if(param != null) {
                param = param.replace(":", "-");

                HashMap<String, AttributeValue> conditionValues = new HashMap<>();

                conditionValues.put(":mac", new AttributeValue().withS(param));

                request.withFilterExpression("macAddress = :mac")
                        .withExpressionAttributeValues(conditionValues);
            }

            ScanResult result = ddb.scan(request);

            LOG.debug("items scanned: " + result);
            for (Map<String, AttributeValue> item : result.getItems()) {
                LOG.debug("item: " + item);

                DeviceResponse deviceResponse = new DeviceResponse(item.get("deviceId").getS(), item.get("macAddress").getS(), item.get("timestamp").getS());

                LOG.debug("DeviceResponse: " + deviceResponse);

                devices.add(deviceResponse);
            }

        } catch (AmazonServiceException ase) {
            LOG.error("Unable to scan table: " + ase.getMessage());
        } catch (AmazonClientException ace) {
            LOG.error("Unable to scan table: " + ace.getMessage());
        } catch (Exception e) {
            LOG.error("Error: " + e.getMessage());
        }

        return devices;
    }

    @Override
    public DeviceResponse getDevice(String deviceId) {
        DeviceResponse device = new DeviceResponse();

        HashMap<String, AttributeValue> key = new HashMap<>();

        key.put("deviceId", new AttributeValue().withS(deviceId));

        GetItemRequest request = new GetItemRequest()
                                        .withKey(key)
                                        .withTableName(devicesTable);

        final AmazonDynamoDB ddb = AmazonDynamoDBClientBuilder.defaultClient();

        try {
            GetItemResult itemResult = ddb.getItem(request);
            LOG.debug("item retrieved: " + itemResult);

            Map<String, AttributeValue> item = itemResult.getItem();

            if (item == null) {
                throw new DatasourceException("Device not found", new Exception("error"));
            }

            device = new DeviceResponse(item.get("deviceId").getS(), item.get("macAddress").getS(), item.get("timestamp").getS());

        } catch (ResourceNotFoundException rnfe) {
            LOG.error("Error: " + rnfe.getMessage());
            throw new DatasourceException("Device not found", rnfe);
        } catch (AmazonServiceException ase) {
            LOG.error("Unable to retrieve item: " + ase.getMessage());
        } catch (AmazonClientException ace) {
            LOG.error("Unable to retrieve item: " + ace.getMessage());
        } catch (Exception e) {
            LOG.error("Error: " + e.getMessage());
            throw new DatasourceException("Device not found", e);
        }

        return device;
    }

    @Override
    public DeviceResponse createDevice(Device device) {
        DeviceResponse newDeviceResponse = null;

        device.setMacAddress(device.getMacAddress().replace(":", "-"));

        try {
            HashMap<String, AttributeValue> itemValues = new HashMap<>();

            itemValues.put("deviceId", new AttributeValue(String.valueOf(device.hashCode())));
            itemValues.put("macAddress", new AttributeValue(device.getMacAddress()));
            itemValues.put("timestamp", new AttributeValue(device.getTimestamp()));

            AmazonDynamoDB ddb = AmazonDynamoDBClientBuilder.defaultClient();

            HashMap<String, AttributeValue> conditionValues = new HashMap<>();

            conditionValues.put(":mac", new AttributeValue(device.getMacAddress()));

            PutItemRequest putItemRequest = new PutItemRequest()
                    .withItem(itemValues)
                    .withTableName(devicesTable)
                    .withConditionExpression("not contains(macAddress, :mac)")
                    .withExpressionAttributeValues(conditionValues)
                    .withReturnValues(ReturnValue.ALL_OLD);


            PutItemResult result = ddb.putItem(putItemRequest);

            LOG.debug("DB result: " + result);

            newDeviceResponse = new DeviceResponse(String.valueOf(device.hashCode()), device.getMacAddress(), device.getTimestamp());

        } catch (ConditionalCheckFailedException cckfe) {
            LOG.error("Duplicated device: " + cckfe.getErrorMessage());
            throw new InvalidInputException("device duplicated", cckfe);
        } catch (AmazonServiceException e) {
            LOG.error("Unable to save device: " + e.getErrorMessage());
        } catch (Exception e) {
            LOG.error("Error: " + e.getMessage());
        }

        return newDeviceResponse;
    }

}
