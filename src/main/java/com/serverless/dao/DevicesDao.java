package com.serverless.dao;

import com.serverless.api.DeviceResponse;
import com.serverless.model.Device;

import java.util.List;

/**
 * Created by DiegoT on 01/11/2018.
 */
public interface DevicesDao {
    List<DeviceResponse> getDevices(String param);
    DeviceResponse getDevice(String deviceId);
    DeviceResponse createDevice(Device device);
}
