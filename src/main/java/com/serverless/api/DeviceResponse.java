package com.serverless.api;

/**
 * Created by DiegoT on 01/11/2018.
 */
public class DeviceResponse {
    private String deviceId;
    private String macAddress;
    private String timestamp;

    public DeviceResponse() {

    }

    public DeviceResponse(String deviceId, String macAddress, String timestamp) {
        this.deviceId = deviceId;
        this.macAddress = macAddress;
        this.timestamp = timestamp;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public String getTimestamp() {
        return timestamp;
    }
}
