package com.serverless.api;

/**
 * Created by DiegoT on 01/11/2018.
 */
public class ErrorResponse {
    private String error;

    public ErrorResponse(String errMsg) {
        this.error = errMsg;
    }

    public String getError() {
        return this.error;
    }
}
