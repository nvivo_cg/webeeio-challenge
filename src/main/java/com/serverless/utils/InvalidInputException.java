package com.serverless.utils;

import com.google.gson.JsonSyntaxException;

/**
 * Created by DiegoT on 01/11/2018.
 */
public class InvalidInputException extends RuntimeException {
    private final String message;

    public InvalidInputException(String msg, Exception e) {
        super(msg, e);
        this.message = msg;
    }

    public String getMessage() {
        return this.message;
    }
}
