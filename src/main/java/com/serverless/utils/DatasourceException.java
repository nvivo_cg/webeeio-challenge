package com.serverless.utils;

/**
 * Created by DiegoT on 01/11/2018.
 */
public class DatasourceException extends RuntimeException {
    private final String message;

    public DatasourceException(String msg, Exception e) {
        super(msg, e);
        this.message = msg;
    }

    public String getMessage() {
        return this.message;
    }

}
