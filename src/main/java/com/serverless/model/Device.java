package com.serverless.model;

public class Device {
	String macAddress;
	String timestamp;

	public Device(String macAddress, String timestamp) {
		this.macAddress = macAddress;
		this.timestamp = timestamp;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public int hashCode() {
		int result = 45 * this.macAddress.hashCode() + this.timestamp.hashCode();

		return result;
	}
}