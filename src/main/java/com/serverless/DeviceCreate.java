package com.serverless;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.JsonObject;
import com.serverless.api.ApiGatewayResponse;
import com.serverless.api.DeviceResponse;
import com.serverless.api.ErrorResponse;
import com.serverless.model.Device;
import com.serverless.service.DeviceInputService;
import com.serverless.service.DevicesService;
import com.serverless.service.impl.DeviceInputServiceImpl;
import com.serverless.service.impl.DevicesServiceImpl;
import com.serverless.utils.DatasourceException;
import com.serverless.utils.InvalidInputException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class DeviceCreate implements RequestHandler<Map<String, Object>, ApiGatewayResponse> {

	private static final Logger LOG = LogManager.getLogger(DeviceCreate.class);

	private DevicesService devicesService = new DevicesServiceImpl();
	private DeviceInputService deviceInputService = new DeviceInputServiceImpl();

	@Override
	public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {
		LOG.info("received: {}", input);

		try {
			Device device = deviceInputService.getDeviceRequest(input);

			DeviceResponse newDevice = devicesService.createDevice(device);

			return ApiGatewayResponse.builder()
					.setStatusCode(200)
					.setObjectBody(newDevice)
					.build();
		} catch (InvalidInputException e) {
			LOG.error("handleRequest - input error: " + e.getMessage(), e);
			return ApiGatewayResponse.builder()
					.setStatusCode(400)
					.setObjectBody(createErrMsg(e.getMessage()))
					.build();
		} catch (DatasourceException dse) {
			LOG.error("handleRequest - datasource error: " + dse.getMessage(), dse);
			return ApiGatewayResponse.builder()
					.setStatusCode(404)
					.build();
		} catch(Exception e) {
			LOG.error("handleRequest: " + e.getMessage(), e);
			String errMsg = "failed to save device";
			return ApiGatewayResponse.builder()
					.setStatusCode(500)
					.setObjectBody(createErrMsg(errMsg))
					.build();
		}
	}

	private ErrorResponse createErrMsg(String msg) {
		ErrorResponse errMsg = new ErrorResponse(msg);

		return errMsg;
	}
}
