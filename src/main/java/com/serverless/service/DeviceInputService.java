package com.serverless.service;

import com.serverless.model.Device;

import java.util.Map;

/**
 * Created by DiegoT on 01/11/2018.
 */
public interface DeviceInputService {
    Device getDeviceRequest(Map<String, Object> input);
    String getDeviceId(Map<String, Object> input);
    String getQueryParam(Map<String, Object> input);
}
