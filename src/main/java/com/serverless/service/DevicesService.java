package com.serverless.service;

import com.serverless.api.DeviceResponse;
import com.serverless.model.Device;

import java.util.List;

/**
 * Created by DiegoT on 01/11/2018.
 */
public interface DevicesService {
    List<DeviceResponse> getDevices(String param);
    DeviceResponse getDevice(String id);
    DeviceResponse createDevice(Device device);
}
