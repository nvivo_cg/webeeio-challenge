package com.serverless.service.impl;

import com.serverless.api.DeviceResponse;
import com.serverless.dao.DevicesDao;
import com.serverless.dao.impl.DevicesDaoImpl;
import com.serverless.model.Device;
import com.serverless.service.DevicesService;
import com.serverless.utils.InvalidInputException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by DiegoT on 01/11/2018.
 */
public class DevicesServiceImpl implements DevicesService {
    private static final Logger LOG = LogManager.getLogger(DevicesServiceImpl.class);

    private DevicesDao devicesDao = new DevicesDaoImpl();

    @Override
    public List<DeviceResponse> getDevices(String param) {
        LOG.debug("calling getDevices");
        return devicesDao.getDevices(param);
    }

    @Override
    public DeviceResponse getDevice(String id) {
        LOG.debug("Calling getDevice");
        return devicesDao.getDevice(id);
    }

    @Override
    public DeviceResponse createDevice(Device device)
    {

        if(LocalDateTime.parse(device.getTimestamp()).toLocalDate().isBefore(LocalDate.of(2018, 1, 1))) {
            LOG.error("Invalid timestamp = " + device.getTimestamp());

            throw new InvalidInputException("Invalid timestamp", new Exception("Error on createDevice"));
        }


        if(!device.getMacAddress().matches("^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$")) {
            LOG.error("Invalid mac address format = " + device.getMacAddress());

            throw new InvalidInputException("Invalid mac address format", new Exception("Error on createDevice"));
        }
        LOG.debug("creating Device service");

        return devicesDao.createDevice(device);
    }
}
