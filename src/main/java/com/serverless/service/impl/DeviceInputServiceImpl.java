package com.serverless.service.impl;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.serverless.model.Device;
import com.serverless.service.DeviceInputService;
import com.serverless.utils.InvalidInputException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

/**
 * Created by DiegoT on 01/11/2018.
 */
public class DeviceInputServiceImpl implements DeviceInputService {
    private static final Logger LOG = LogManager.getLogger(DeviceInputServiceImpl.class);

    @Override
    public Device getDeviceRequest(Map<String, Object> input) {
        LOG.debug("starting with input=" + input);

        Device device = new Device(getBodyProperty(input, "macAddress").getAsString(), getBodyProperty(input, "timestamp").getAsString());

        return device;
    }

    @Override
    public String getDeviceId(Map<String, Object> input) {
        String deviceId = getPathParam(input, "deviceId").getAsString();

        return deviceId;
    }

    @Override
    public String getQueryParam(Map<String, Object> input) {
        String macAddress = null;

        try {
            macAddress = getQueryParam(input, "macAddress").getAsString();
        } catch (InvalidInputException e) {
            LOG.error("query param not found");
        } catch (Exception e) {
            LOG.error("query param not found");
        }

        return macAddress;
    }

    private JsonElement getBodyProperty(Map<String, Object> input, String property) {
        try {
            JsonObject pathParameters = new JsonParser().parse(input.get("body").toString()).getAsJsonObject();

            return pathParameters.get(property);
        } catch (JsonSyntaxException e) {
            throw new InvalidInputException("internal server error - failed to parse input body", e);
        } catch (Exception e) {
            throw new InvalidInputException("internal server error - failed to parse input body", e);
        }
    }

    private JsonElement getPathParam(Map<String, Object> input, String param) {
        try {
            JsonObject pathParameters = new JsonParser().parse(input.get("pathParameters").toString()).getAsJsonObject();
            return pathParameters.get(param);
        } catch (JsonSyntaxException e) {
            throw new InvalidInputException("internal server error - failed to parse input path param", e);
        } catch (Exception e) {
            throw new InvalidInputException("internal server error - failed to parse input body", e);
        }
    }

    private JsonElement getQueryParam(Map<String, Object> input, String param) {
        try {
            JsonObject pathParameters = new JsonParser().parse(input.get("queryStringParameters").toString()).getAsJsonObject();

            LOG.debug("query param=" + pathParameters);
            return pathParameters.get(param);
        } catch (JsonSyntaxException e) {
            throw new InvalidInputException("internal server error - failed to parse input query param", e);
        } catch (Exception e) {
            throw new InvalidInputException("internal server error - failed to parse input query param", e);
        }
    }

}
